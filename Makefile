# Makefile

include config.mk

CC=gcc
CFLAGS=-W -Wall -Wextra -Werror -pedantic -ansi
EXECUTABLE=kbacklight
EXECUTABLE_TEST=kbacklight_test
SRC_MAIN=src/main
SRC_TEST=src/test
SRC_BIN=bin

all: package test

package: kbacklight
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE) $(SRC_BIN)/main.o $(SRC_BIN)/version.o $(SRC_BIN)/errors.o $(SRC_BIN)/setup.o $(SRC_BIN)/brightness.o

test: package kbacklight_test
	$(CC) -o $(SRC_BIN)/$(EXECUTABLE_TEST) $(SRC_BIN)/main.o

kbacklight: init main.o version.o errors.o setup.o brightness.o
	$(CC) -o $(SRC_BIN)/kbacklight $(SRC_BIN)/main.o $(SRC_BIN)/version.o $(SRC_BIN)/errors.o $(SRC_BIN)/setup.o $(SRC_BIN)/brightness.o $(CFLAGS)

kbacklight_test: $(SRC_TEST)/main.c
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_TEST)/main.c $(CFLAGS)

init:
	mkdir -p bin

main.o: $(SRC_MAIN)/main.c $(SRC_MAIN)/config.h $(SRC_MAIN)/errors.h $(SRC_MAIN)/setup.h $(SRC_MAIN)/brightness.h
	$(CC) -o $(SRC_BIN)/main.o -c $(SRC_MAIN)/main.c $(CFLAGS)

version.o: $(SRC_MAIN)/version.c $(SRC_MAIN)/version.h
	$(CC) -o $(SRC_BIN)/version.o -c $(SRC_MAIN)/version.c $(CFLAGS)

errors.o: $(SRC_MAIN)/errors.c $(SRC_MAIN)/errors.h
	$(CC) -o $(SRC_BIN)/errors.o -c $(SRC_MAIN)/errors.c $(CFLAGS)

setup.o: $(SRC_MAIN)/setup.c $(SRC_MAIN)/setup.h $(SRC_MAIN)/config.h $(SRC_MAIN)/version.h
	$(CC) -o $(SRC_BIN)/setup.o -c $(SRC_MAIN)/setup.c $(CFLAGS)

brightness.o: $(SRC_MAIN)/brightness.c $(SRC_MAIN)/brightness.h $(SRC_MAIN)/config.h $(SRC_MAIN)/errors.h
	$(CC) -o $(SRC_BIN)/brightness.o -c $(SRC_MAIN)/brightness.c $(CFLAGS)

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f $(SRC_BIN)/$(EXECUTABLE) ${DESTDIR}${PREFIX}/bin
	chown root:root ${DESTDIR}${PREFIX}/bin/$(EXECUTABLE)
	chmod 4755 ${DESTDIR}${PREFIX}/bin/$(EXECUTABLE)

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/$(EXECUTABLE)

clean:
	rm -f $(SRC_BIN)/*.o
	rm -f $(SRC_BIN)/$(EXECUTABLE)

